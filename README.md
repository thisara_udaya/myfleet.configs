# About Code #

- This is the application configuration of all the microservices maintained as a git repo.

## Tech Stack 

- Git

## Setup Guide 

1. Clone the Code : `` git clone git@bitbucket.org:thisara_udaya/myfleet.configs.git -b expnval ``

2. Select Branch : `` git branch master ``

3. Checkout Branch : `` git checkout master ``

Note : The config server is configured to read from the master branch.
	
#### Demo video link :  https://www.youtube.com/watch?v=Lkq9-srSWbc